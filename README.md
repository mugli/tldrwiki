# tldrwiki

API service for fetching description on an entity using Wikipedia API.

---

## Run

Using docker:

```sh
make run
```

## OpenAPI Schema

Open <http://localhost:3000/swagger/index.html> in the browser for the Swagger UI.
(The root path http://localhost:3000/ also redirects to this URL.)

---

## Development

Run tests:

```sh
make test
```

---

## REST API

Fetch description on an entity/title using Wikipedia API:

> `GET http://localhost:3000/api/pages/{title}`

---

## Usage Examples

### 1. Fetch short description for a page

```sh
curl -i 'http://localhost:3000/api/pages/Yoshua%20Bengio'
```

Returns:

```sh
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Date: Sat, 01 Oct 2022 16:46:56 GMT
Content-Length: 70

{"title":"Yoshua Bengio","description":"Canadian computer scientist"}
```

Notes:

- Not all pages in English Wikipedia have short description though. In those cases empty string `""` will be returned for `description`.

- `title` field always contains title from the page. It can be normalized and different from the url. For example, `http://localhost:3000/api/pages/Yoshua_Bengio` will return normalized title `"Yoshua Bengio"` without the underscore.

### 2. Show suggestion (if available) for misspelling/missing page

```sh
curl -i 'http://localhost:3000/api/pages/yoshua_bbengio'
```

Returns:

```sh
HTTP/1.1 404 Not Found
Content-Type: application/json; charset=utf-8
Date: Sat, 01 Oct 2022 16:48:01 GMT
Content-Length: 54

{"error":"Not Found","suggestions":["Yoshua Bengio"]}
```

Suggestions are fetched using the mediawiki [Prefixsearch API](https://www.mediawiki.org/wiki/API:Prefixsearch).

### 3. Follow [`Wikipedia:Redirect`](https://en.wikipedia.org/wiki/Wikipedia:Redirect)

The `wikiclient` can detect and transparently follow redirects defined in page content using `#REDIRECT [[Name of article]]` format.

This examples follows redirects from `mandela` to `Nelson Mandela` transparently and fetches the content:

```sh
curl -i 'http://localhost:3000/api/pages/mandela'
```

Returns:

```sh
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Date: Sat, 01 Oct 2022 16:48:52 GMT
Content-Length: 93

{"title":"Nelson Mandela","description":"First president of South Africa from 1994 to 1999"}
```

Redirects are followed at-most once.

---

## Production use considerations (TODO)


### Scalability

- The service is stateless and can be scaled horizontally using replicas and placed behind a load-balancer
- Server-side cache and/or CDN
- Client-side HTTP caching using headers
- Reuse connection pool for connecting to Wikimedia API. This helps avoid port exhaustion during load.

### Maintainability/discoverability

- OpenAPI schema (_done_)
- More unit tests, some integration/contract tests (if applicable)
- Add linter
- Setup CI/CD
- Better documentation, add more godoc etc

### Resiliency (for the API server)

- Add rate-limits to the API
- Add throttling after max concurrent connection
- Handle timeouts (_done_)

### Resiliency (for upstream)

- Back-off and retry on rate-limit (if applicable, depending on upstream API)
- Circuit breaking when upstream service is experiencing downtime

### Observability

- Add tracing
- Add metrics
- Add structured logging (_done_)

### Other

- Use HTTPS

---

## Known limitations

- `{{short description|wikidata}}` is not supported yet. This service does not use `wikidata` API in this case and returns 501 Not Implemented error in this case.

---