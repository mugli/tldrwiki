package main

import (
	"context"
	"errors"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/rs/zerolog"
	"gitlab.com/mugli/tldrwiki/api"
	"gitlab.com/mugli/tldrwiki/logger"
	"gitlab.com/mugli/tldrwiki/wikiclient"

	"net/http"
)

const (
	defaultHTTPAddr    = ":3000"
	gracefulTimeoutSec = 5
)

func main() {
	log := logger.New(os.Stderr, zerolog.DebugLevel)

	if err := bootstrap(log); err != nil {
		log.Fatal().Err(err).Msg("server bootstrap failed")
	}
}

// bootstrap the application
func bootstrap(log *zerolog.Logger) error {
	// init app, routes, handlers etc
	client := wikiclient.NewClient()
	api := api.New(client, log)
	server := NewServer(api)

	// Create a channel that listens on incoming interrupt signals
	signalChan := make(chan os.Signal, 1)
	signal.Notify(
		signalChan,
		syscall.SIGHUP,  // kill -SIGHUP XXXX
		syscall.SIGINT,  // kill -SIGINT XXXX or Ctrl+c
		syscall.SIGQUIT, // kill -SIGQUIT XXXX
	)

	// Graceful shutdown
	go func() {
		// Wait for a new signal on channel
		<-signalChan
		// Signal received, shutdown the server
		log.Info().Msg("shutting down..")

		// Create context with timeout
		ctx, cancel := context.WithTimeout(context.Background(), gracefulTimeoutSec*time.Second)
		defer cancel()
		server.Shutdown(ctx)

		// Check if context timeouts, in worst case call cancel via defer
		select {
		case <-time.After((gracefulTimeoutSec + 1) * time.Second):
			log.Info().Msg("not all connections closed")
		case <-ctx.Done():
		}
	}()

	// start the http server
	log.Info().Msg("starting http server: http://" + defaultHTTPAddr)
	err := server.ListenAndServe()
	if err != nil && !errors.Is(err, http.ErrServerClosed) {
		return err
	}

	return nil
}

// NewServer creates a http server
func NewServer(api *api.API) *http.Server {
	return &http.Server{
		Addr:         defaultHTTPAddr,
		WriteTimeout: time.Second * 10,
		ReadTimeout:  time.Second * 10,
		IdleTimeout:  time.Second * 60,
		Handler:      api.Router(),
	}
}
