package api

import (
	"context"
	"errors"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gitlab.com/mugli/tldrwiki/wikiclient"
)

type titleSuccessResponse struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

type titleNotFoundErrorResponse struct {
	Error       string   `json:"error"`
	Suggestions []string `json:"suggestions,omitempty"`
}

type titleErrorResponse struct {
	Error string `json:"error"`
}

// GetPageHandler handles /api/pages/{title} route
// @Summary Get basic Page info from Wikipedia
// @Description Get basic Page info from Wikipedia
// @ID get-string-by-int
// @Accept  json
// @Produce  json
// @Param   title      path   string     true  "Example: Yoshua Bengio"
// @Success 200 {object} titleSuccessResponse
// @Failure 404 {object} titleNotFoundErrorResponse
// @Failure 501 {object} titleErrorResponse
// @Failure 503 {object} titleErrorResponse
// @Failure 500 {object} titleErrorResponse
// @Router /api/pages/{title} [get]
func (api *API) GetPageHandler(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	title := strings.TrimSpace(chi.URLParam(r, "title"))

	page, err := api.WikiClient.GetPage(ctx, title, true)

	if err != nil {
		if errors.Is(err, wikiclient.ErrPageNotFound) {
			handlePageNotFoundError(ctx, api, title, r, w)
			return
		} else if errors.Is(err, wikiclient.ErrUnsupportedShortDesc) {
			handleGenericHTTPError(r, w, http.StatusNotImplemented)
			return
		} else if errors.Is(err, wikiclient.ErrServiceUnavailable) {
			api.Log.Error().Err(err).Msg("error getting page info")
			handleGenericHTTPError(r, w, http.StatusServiceUnavailable)
			return
		} else {
			api.Log.Error().Err(err).Msg("unexpected error getting page info")
			handleGenericHTTPError(r, w, http.StatusInternalServerError)
			return
		}
	}

	response := titleSuccessResponse{
		Title:       page.Title,
		Description: page.Description,
	}
	render.Status(r, http.StatusOK)
	render.JSON(w, r, response)
}

func handleGenericHTTPError(r *http.Request, w http.ResponseWriter, status int) {
	errResponse := titleErrorResponse{
		Error: http.StatusText(status),
	}
	render.Status(r, status)
	render.JSON(w, r, errResponse)
}

func handlePageNotFoundError(ctx context.Context, api *API, title string, r *http.Request, w http.ResponseWriter) {
	suggestions, _ := api.WikiClient.Search(ctx, title)

	errResponse := titleNotFoundErrorResponse{
		Error:       http.StatusText(http.StatusNotFound),
		Suggestions: suggestions,
	}
	render.Status(r, http.StatusNotFound)
	render.JSON(w, r, errResponse)
}
