package api

import (
	"net/http"

	"github.com/rs/zerolog"
	"gitlab.com/mugli/tldrwiki/wikiclient"
)

type API struct {
	HttpSrv    *http.Server
	Log        *zerolog.Logger
	WikiClient *wikiclient.Client
}

func New(wikiClient *wikiclient.Client, log *zerolog.Logger) *API {
	return &API{
		WikiClient: wikiClient,
		Log:        log,
	}
}
