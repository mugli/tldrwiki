package api

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/httplog"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab.com/mugli/tldrwiki/docs"
)

// @title tldrwiki API
// @version 1.0
// @description API for fetching description on an entity using Wikipedia API

// @contact.name API Support
// @contact.email mhasan@omicronlab.com

// @BasePath /
func (app *API) Router() *chi.Mux {
	router := chi.NewRouter()
	httplog.DefaultOptions.Concise = true

	router.Use(middleware.RequestID)
	router.Use(app.Recoverer)
	router.Use(httplog.Handler(*app.Log))

	// api only server. redirect to documentation from the root
	router.Get("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/swagger/", http.StatusMovedPermanently)
	}))

	router.Route("/api", func(r chi.Router) {
		r.Get("/pages/{title}", app.GetPageHandler)

		// render JSON output for missing routes inside /api for consistency
		r.NotFound(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			handleGenericHTTPError(r, w, http.StatusNotFound)
		}))
	})

	router.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("/swagger/doc.json"),
	))

	return router
}
