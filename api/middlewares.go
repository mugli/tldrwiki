package api

import (
	"net/http"
	"runtime/debug"
)

// Recoverer middleware recovers from panic
func (app *API) Recoverer(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if rvr := recover(); rvr != nil && rvr != http.ErrAbortHandler {

				app.Log.Error().Timestamp().Interface("recover_info", rvr).Bytes("debug_stack", debug.Stack()).Msg("panic_request")
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
		}()

		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}
