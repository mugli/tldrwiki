package wikiclient

import (
	"context"
	"net/http"
)

type wikiQueryResponse struct {
	Warnings struct {
		Main struct {
			Warnings string `json:"warnings"`
		} `json:"main"`
		Revisions struct {
			Warnings string `json:"warnings"`
		} `json:"revisions"`
	} `json:"warnings"`
	Query struct {
		Pages []struct {
			Pageid    int    `json:"pageid"`
			Title     string `json:"title"`
			Missing   bool   `json:"missing"`
			Revisions []struct {
				Contentformat string `json:"contentformat"`
				Contentmodel  string `json:"contentmodel"`
				Content       string `json:"content"`
			} `json:"revisions"`
		} `json:"pages"`
	} `json:"query"`
}

type Page struct {
	Title       string `json:"title"`
	Description string `json:"description"`
}

func (c *Client) GetPage(ctx context.Context, title string, followRedirects bool) (Page, error) {
	response, err := makeRequest(ctx, title, c)
	if err != nil {
		return Page{}, err
	}

	content, err := extractContent(response)
	if err != nil {
		return Page{}, err
	}

	if followRedirects {
		// Handle redirect once, if necessary
		// More info: https://en.wikipedia.org/wiki/Wikipedia:Redirect
		// Example: https://en.wikipedia.org/w/api.php?action=query&prop=revisions&titles=mandella&rvlimit=1&formatversion=2&format=json&rvprop=content
		needToRedirect, newTitle := shouldRedirect(content)
		if needToRedirect {
			response, err = makeRequest(ctx, newTitle, c)
			if err != nil {
				return Page{}, err
			}

			content, err = extractContent(response)
			if err != nil {
				return Page{}, err
			}
		}
	}

	description, err := extractShortDesc(content)
	if err != nil {
		return Page{}, err
	}

	return Page{
		Title:       response.Query.Pages[0].Title,
		Description: description,
	}, nil
}

func makeRequest(ctx context.Context, title string, c *Client) (wikiQueryResponse, error) {
	req, err := http.NewRequest("GET", c.BaseURL, nil)
	if err != nil {
		return wikiQueryResponse{}, err
	}

	q := req.URL.Query()
	q.Add("titles", title)
	q.Add("action", "query")
	q.Add("prop", "revisions")
	q.Add("rvlimit", "1")
	q.Add("format", "json")
	q.Add("formatversion", "2")
	q.Add("rvprop", "content")
	req.URL.RawQuery = q.Encode()

	req = req.WithContext(ctx)

	res := wikiQueryResponse{}
	if err := c.sendRequest(req, &res); err != nil {
		return wikiQueryResponse{}, err
	}

	return res, nil
}

func extractContent(response wikiQueryResponse) (string, error) {

	if len(response.Query.Pages) <= 0 {
		return "", nil
	} else if response.Query.Pages[0].Missing {
		return "", ErrPageNotFound
	} else if len(response.Query.Pages[0].Revisions) <= 0 {
		return "", nil
	}

	content := response.Query.Pages[0].Revisions[0].Content

	return content, nil
}
