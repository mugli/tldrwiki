package wikiclient

import "errors"

var ErrPageNotFound = errors.New("page not found")
var ErrUnsupportedShortDesc = errors.New("unsupported short description")
var ErrServiceUnavailable = errors.New("service unavailable")
