package wikiclient

import (
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"time"
)

var (
	BaseURLV1        = "https://en.wikipedia.org/w/api.php"
	TransportTimeout = 5 * time.Second
	HttpTimeout      = 10 * time.Second
)

type Client struct {
	BaseURL    string
	HTTPClient *http.Client
}

func NewClient() *Client {
	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Timeout: TransportTimeout,
		}).DialContext,
		TLSHandshakeTimeout:   TransportTimeout,
		ResponseHeaderTimeout: TransportTimeout,
	}

	return &Client{
		BaseURL: BaseURLV1,
		HTTPClient: &http.Client{
			Timeout:   HttpTimeout,
			Transport: transport,
		},
	}
}

// There is no hard and fast limit on read requests
// More: https://www.mediawiki.org/wiki/API:Etiquette
// T&C: https://www.mediawiki.org/wiki/Wikimedia_REST_API#Terms_and_conditions
func (c *Client) sendRequest(req *http.Request, result interface{}) error {
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.Header.Set("User-Agent", "tldrwiki_Client/1.0 (mhasan@omicronlab.com)")

	res, err := c.HTTPClient.Do(req)
	if err != nil {
		if os.IsTimeout(err) {
			return ErrServiceUnavailable
		}

		return err
	}

	defer res.Body.Close()

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		return fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(body, result); err != nil {
		return err
	}

	return nil
}
