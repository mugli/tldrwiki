package wikiclient

import (
	"context"
	"net/http"
)

type wikiSearchResponse struct {
	Query struct {
		Prefixsearch []struct {
			Title string `json:"title"`
		} `json:"prefixsearch"`
	} `json:"query"`
}

func (c *Client) Search(ctx context.Context, title string) ([]string, error) {
	req, err := http.NewRequest("GET", c.BaseURL, nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Add("pssearch", title)
	q.Add("action", "query")
	q.Add("list", "prefixsearch")
	q.Add("format", "json")
	req.URL.RawQuery = q.Encode()

	req = req.WithContext(ctx)

	searchResponse := wikiSearchResponse{}
	if err := c.sendRequest(req, &searchResponse); err != nil {
		return nil, err
	}

	result := []string{}
	for _, searchResult := range searchResponse.Query.Prefixsearch {
		result = append(result, searchResult.Title)
	}

	return result, nil
}
