package wikiclient

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func Test_extractShortDesc(t *testing.T) {
	type result struct {
		Description string
		Err         error
	}

	tests := map[string]struct {
		input string
		want  result
	}{
		"no description":                              {input: "Lorem ipsum dolor sit amet", want: result{"", nil}},
		"no description 1":                            {input: "{{Short description}}", want: result{"", nil}},
		"no description 2":                            {input: "{{Short description|}}", want: result{"", nil}},
		"no description with none":                    {input: "{{Short description|none}}", want: result{"", nil}},
		"no description with none, case insensitive":  {input: "{{Short description|NONE}}", want: result{"", nil}},
		"throw ErrUnsupportedShortDesc with wikidata": {input: "{{Short description|wikidata}}", want: result{"", ErrUnsupportedShortDesc}},
		"throw ErrUnsupportedShortDesc with wikidata, , case insensitive": {input: "{{Short description|WIKIDATA}}", want: result{"", ErrUnsupportedShortDesc}},
		"extract description":                                   {input: "Lorem ipsum dolor {{Short description|Use of oxygen as medical treatment}} Lorem ipsum dolor", want: result{"Use of oxygen as medical treatment", nil}},
		"extract description, case insensitive":                 {input: "Lorem ipsum dolor {{SHORT DESCRIPTION|Use of oxygen as medical treatment}} Lorem ipsum dolor", want: result{"Use of oxygen as medical treatment", nil}},
		"extract description, trim whitespace":                  {input: "Lorem ipsum dolor {{ 		  SHORT DESCRIPTION  		|Use of oxygen as medical treatment}} Lorem ipsum dolor", want: result{"Use of oxygen as medical treatment", nil}},
		"extract description, trim whitespace from description": {input: "Lorem ipsum dolor {{ 		  SHORT DESCRIPTION  		|		   Use of oxygen as medical treatment  		}} Lorem ipsum dolor", want: result{"Use of oxygen as medical treatment", nil}},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			description, err := extractShortDesc(tc.input)
			got := result{description, err}

			diff := cmp.Diff(tc.want, got, cmpopts.EquateErrors())

			if diff != "" {
				t.Fatal(diff)
			}
		})
	}
}

func Test_shouldRedirect(t *testing.T) {
	type result struct {
		NeedToRedirect bool
		NewTitle       string
	}
	tests := map[string]struct {
		input string
		want  result
	}{
		"no redirect":                            {input: "Lorem ipsum dolor sit amet", want: result{false, ""}},
		"no redirect on empty new title":         {input: "#REDIRECT [[]]", want: result{false, ""}},
		"no redirect on unmatched brackets":      {input: "#REDIRECT [[]", want: result{false, ""}},
		"redirect":                               {input: "#REDIRECT [[Nelson Mandela]]", want: result{true, "Nelson Mandela"}},
		"redirect, case insensitive":             {input: "#redirect [[Nelson Mandela]]", want: result{true, "Nelson Mandela"}},
		"redirect, trim whitespace":              {input: "		  #redirect [[Nelson Mandela]]  		", want: result{true, "Nelson Mandela"}},
		"redirect, trim everything":              {input: "Lorem		  #redirect [[Nelson Mandela]]  		ipsum", want: result{true, "Nelson Mandela"}},
		"redirect, trim whitespace of new title": {input: "Lorem		  #redirect [[  Nelson Mandela		]]  		ipsum", want: result{true, "Nelson Mandela"}},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			needToRedirect, newTitle := shouldRedirect(tc.input)
			got := result{needToRedirect, newTitle}

			diff := cmp.Diff(tc.want, got)

			if diff != "" {
				t.Fatal(diff)
			}
		})
	}
}
