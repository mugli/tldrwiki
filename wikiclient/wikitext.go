package wikiclient

import (
	"regexp"
	"strings"
)

var (
	// doc: https://en.wikipedia.org/wiki/Template:Short_description
	shortDescRegexp = regexp.MustCompile(`(?mi){{\s*Short description\s*\|([^}]*)}}`)

	// doc: https://en.wikipedia.org/wiki/Wikipedia:Redirect
	redirectRegexp = regexp.MustCompile(`(?mi)#REDIRECT\s*\[\[([^]]*)]]`)
)

func extractShortDesc(content string) (string, error) {
	matches := shortDescRegexp.FindStringSubmatch(content)

	if matches == nil || len(matches) < 2 {
		return "", nil
	}

	desc := strings.TrimSpace(matches[1])

	if "none" == strings.ToLower(desc) {
		return "", nil
	} else if "wikidata" == strings.ToLower(desc) {
		return "", ErrUnsupportedShortDesc
	}

	return desc, nil
}

func shouldRedirect(content string) (bool, string) {
	matches := redirectRegexp.FindStringSubmatch(content)

	if matches == nil || len(matches) < 2 {
		return false, ""
	}

	newTitle := strings.TrimSpace(matches[1])

	if newTitle == "" {
		return false, ""
	}

	return true, newTitle
}
