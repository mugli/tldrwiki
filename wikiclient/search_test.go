package wikiclient_test

import (
	"context"
	"net/http"
	"path"
	"testing"

	"github.com/google/go-cmp/cmp"
	"gitlab.com/mugli/tldrwiki/wikiclient"
	"gopkg.in/h2non/gock.v1"
)

// TODO: test error cases: https://en.wikipedia.org/w/api.php?action=query&list=prefixsearch&pssearch=&format=json

func Test_Search(t *testing.T) {
	t.Cleanup(func() {
		gock.EnableNetworking()
		gock.OffAll()
	})

	gock.DisableNetworking()

	type input struct {
		ctx   context.Context
		title string
	}

	type output struct {
		result  []string
		withErr bool
	}

	tests := []struct {
		name   string
		setup  func()
		input  input
		output output
	}{
		{
			"200",
			func() {
				gock.New("https://en.wikipedia.org/w/api.php").
					MatchParams(map[string]string{
						"pssearch": "Star Wars",
						"action":   "query",
						"list":     "prefixsearch",
						"format":   "json",
					}).
					Get("/").
					Reply(http.StatusOK).
					File(path.Join("fixtures", "search", "200.json"))
			},
			input{
				context.Background(),
				"Star Wars",
			},
			output{
				result:  []string{"Star Wars", "Star Wars (film)", "Star Wars: The Rise of Skywalker"},
				withErr: false,
			},
		}}

	client := wikiclient.NewClient()
	gock.InterceptClient(client.HTTPClient)

	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			tt.setup()

			actualRes, actualErr := client.Search(tt.input.ctx, tt.input.title)

			if (actualErr != nil) != tt.output.withErr {
				t.Fatalf("expected error %t, actual %s", tt.output.withErr, actualErr)
			}

			if !cmp.Equal(tt.output.result, actualRes) {
				t.Fatalf("expected output do not match\n%s", cmp.Diff(tt.output.result, actualRes))
			}
		})
	}

	// verify that we don't have pending mocks
	if !gock.IsDone() {
		t.Fatal("Pending mock found")
	}
}
