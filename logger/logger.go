package logger

import (
	"io"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/pkgerrors"
)

func New(output io.Writer, logLevel zerolog.Level) *zerolog.Logger {
	zerolog.SetGlobalLevel(logLevel)
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack

	logger := zerolog.New(output).With().Timestamp().Logger()

	return &logger
}
