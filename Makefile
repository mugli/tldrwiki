.PHONY: swag run test

dep:
	go mod download
	go mod tidy

run: dep
	docker build -t tldrwiki . && docker run -it --rm -p 3000:3000 tldrwiki

test: dep
	go test ./... -coverprofile cover.out

coverage: test
	go tool cover -html=cover.out

swag: # regenerate swagger docs
	swag init -g api/router.go --parseDependency --parseInternal